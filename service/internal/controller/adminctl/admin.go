package adminctl

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/auth"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/stock"
	"go.uber.org/dig"
)

type adminControllerSet struct {
	dig.In

	AuthToken auth.IToken
	Stock     stock.ITWStock
}

func NewController(set adminControllerSet) *Controller {
	return &Controller{
		HttpAuthToken: newAuthTokenHttpController(set.AuthToken),
		Stock:         newStockController(set.Stock),
	}
}

type Controller struct {
	HttpAuthToken IAuthTokenHttpController
	Stock         IStockController
}
