package adminctl

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/rbac"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/handler"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/auth"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/ctxs"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

func newAuthTokenHttpController(token auth.IToken) IAuthTokenHttpController {
	return &authTokenHttpController{
		token: token,
	}
}

type IAuthTokenHttpController interface {
	MiddlewareAuth(ctx *gin.Context)
	MiddlewareRBAC(ctx *gin.Context)
}

type authTokenHttpController struct {
	token auth.IToken
}

func (at *authTokenHttpController) MiddlewareAuth(ctx *gin.Context) {
	sc := ctxs.GetSelfContext(ctx)

	token := ctx.GetHeader("Authorization")

	adminRepository, err := at.token.AuthAdmin(sc, token)
	if err != nil {
		handler.Ctx.Response(
			ctx,
			http.StatusUnauthorized,
			handler.ContentTypeJson,
			handler.Error.Marshal(errs.AuthTokenUnauthorized),
		)
		ctx.Abort()
		return
	}

	sc = ctxs.SetAdminID(sc, adminRepository.ID)
	sc = ctxs.SetToken(sc, adminRepository.Token)
	ctx = ctxs.SetSelfContext(sc, ctx).(*gin.Context)

	ctx.Next()
}

func (at *authTokenHttpController) MiddlewareRBAC(ctx *gin.Context) {
	sc := ctxs.GetSelfContext(ctx)
	id, exist := ctxs.GetAdminID(sc)
	if !exist {
		logger.ApLog().Debug("id not exist")
		handler.Ctx.Response(
			ctx,
			http.StatusUnauthorized,
			handler.ContentTypeJson,
			handler.Error.Marshal(errs.AuthTokenUnauthorized),
		)
		ctx.Abort()
		return
	}
	logger.ApLog().Debug(id)

	e := rbac.GetAdminAuthInstance()
	idStr := strconv.FormatInt(id, 10)
	hasRight, err := e.Enforce(idStr, ctx.Request.URL.Path, ctx.Request.Method)
	if err != nil {
		logger.ApLog().Debug("ops forbidden")
		handler.Ctx.Response(
			ctx,
			http.StatusUnauthorized,
			handler.ContentTypeJson,
			handler.Error.Marshal(errs.AuthOperationForbidden),
		)
		ctx.Abort()
		return
	}
	if hasRight {
		ctx.Next()
	} else {
		logger.ApLog().Debug("ops forbidden")
		handler.Ctx.Response(
			ctx,
			http.StatusForbidden,
			handler.ContentTypeJson,
			handler.Error.Marshal(errs.AuthOperationForbidden),
		)
		ctx.Abort()
		return
	}
}
