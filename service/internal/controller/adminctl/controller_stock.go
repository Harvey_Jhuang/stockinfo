package adminctl

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/handler"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/stock"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/condition"
)

func newStockController(stock stock.ITWStock) IStockController {
	return &stockController{
		stock: stock,
	}
}

type IStockController interface {
	AddInfoJob(ctx *gin.Context)
	AddIndexJob(ctx *gin.Context)
}

type stockController struct {
	stock stock.ITWStock
}

func (c *stockController) AddInfoJob(ctx *gin.Context) {
	var req condition.StockJobReq
	if err := handler.Ctx.BindJson(ctx, &req); err != nil {
		logger.ApLog().Errorf("bind cond err: %v", err)
		handler.Ctx.ResponseJsonBadRequest(ctx, err)
		return
	}

	resp, err := c.stock.ExecDailyCloseInfo(&req)
	if err != nil {
		handler.Ctx.ResponseJsonBadRequest(ctx, err)
		return
	}

	if err := handler.Ctx.ResponseJsonStatusOK(ctx, resp); err != nil {
		logger.ApLog().Errorf("marshal data err: %v", err)
		handler.Ctx.ResponseJsonBadRequest(ctx, err)
		return
	}
}

func (c *stockController) AddIndexJob(ctx *gin.Context) {
	var req condition.StockJobReq
	if err := handler.Ctx.BindJson(ctx, &req); err != nil {
		logger.ApLog().Errorf("bind cond err: %v", err)
		handler.Ctx.ResponseJsonBadRequest(ctx, err)
		return
	}

	resp, err := c.stock.ExecDailyMarketIndex(&req)
	if err != nil {
		logger.ApLog().Error(err)
		handler.Ctx.ResponseJsonBadRequest(ctx, err)
		return
	}

	if err := handler.Ctx.ResponseJsonStatusOK(ctx, resp); err != nil {
		logger.ApLog().Errorf("marshal data err: %v", err)
		handler.Ctx.ResponseJsonBadRequest(ctx, err)
		return
	}
}
