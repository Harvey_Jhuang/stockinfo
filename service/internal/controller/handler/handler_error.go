package handler

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/tools"
)

func newErrorHandler() ErrorHandler {
	return ErrorHandler{}
}

type ErrorHandler struct {
}

func (eh ErrorHandler) Parse(err error) *errs.Error {
	e, ok := errs.Parse(err)
	if !ok {
		tmp := (errs.CommonUnknownError).(*errs.Error)
		return tmp
	}

	return e
}

func (eh ErrorHandler) Marshal(err error) []byte {
	result, marshalErr := tools.JsonMarshal(Error.Parse(err))
	if marshalErr != nil {
		return []byte{}
	}

	return result
}
