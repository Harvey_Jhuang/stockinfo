package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"runtime/debug"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/pkg/errors"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/ctxs"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/tools"
)

const (
	ContentTypeProtoBuf = "application/x-protobuf"
	ContentTypeJson     = "application/json; charset=utf-8"
)

func newCtxHandler() CtxHandler {
	return CtxHandler{}
}

type CtxHandler struct {
}

func (eh CtxHandler) ResponseJsonStatusOK(ctx interface{}, payload interface{}) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.Errorf("%s\n %s", e, debug.Stack())
			return
		}
	}()

	respBuf, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	eh.ResponseStatusOK(ctx, ContentTypeJson, respBuf)

	return nil
}

func (eh CtxHandler) ResponseStatusOK(ctx interface{}, contentType string, payload []byte) error {
	return eh.Response(ctx, http.StatusOK, contentType, payload)
}

func (eh CtxHandler) Response(ctx interface{}, status int, contentType string, payload []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.Errorf("%s\n %s", e, debug.Stack())
			return
		}
	}()

	switch t := ctx.(type) {
	case *gin.Context:
		t.Data(status, contentType, payload)
	default:
		m := fmt.Sprintf("%s, type: %v", errs.FrameworkContextErrorType, t)
		panic(m)
	}

	eh.loggerResponse(ctx, payload)

	return nil
}

func (eh CtxHandler) ResponseJsonBadRequest(ctx interface{}, errMsg error) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.Errorf("%s\n %s", e, debug.Stack())
			return
		}
	}()

	respBuf, err := json.Marshal(Error.Parse(errMsg))
	if err != nil {
		return err
	}

	eh.Response(ctx, http.StatusBadRequest, ContentTypeJson, respBuf)

	return nil
}

func (eh CtxHandler) BindJson(ctx interface{}, obj interface{}) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.Errorf("%s\n %s", e, debug.Stack())
			return
		}
	}()

	switch t := ctx.(type) {
	case *gin.Context:
		switch t.Request.Method {
		case http.MethodGet:
			return eh.bindGinMethodGet(t, obj)
		default:
			if err := binding.JSON.Bind(t.Request, obj); err != nil {
				logger.ApLog().Errorf("%s: %s", errs.FrameworkRequestBodyParseError, err)
				return errs.FrameworkRequestBodyParseError
			}

		}
	default:
		m := fmt.Sprintf("%s, type: %v", errs.FrameworkContextErrorType, t)
		panic(m)
	}

	eh.loggerRequest(ctx, obj)

	return nil
}

func (eh CtxHandler) bindGinMethodGet(ctx *gin.Context, obj interface{}) (err error) {
	if err := binding.Query.Bind(ctx.Request, obj); err != nil {
		logger.ApLog().Errorf("%s: %s", errs.FrameworkRequestBodyParseError, err)
		return errs.FrameworkRequestBodyParseError
	}
	return nil
}

func (eh CtxHandler) GetParamInt32(ctx interface{}, key string) (int32, error) {
	val, err := eh.getParam(ctx, key)
	if err != nil {
		return 0, nil
	}

	v, err := strconv.ParseInt(val, 10, 32)
	if err != nil {
		return 0, errs.CommonRequestParamParseFailed
	}

	return int32(v), nil
}

func (eh CtxHandler) GetParamInt64(ctx interface{}, key string) (int64, error) {
	val, err := eh.getParam(ctx, key)
	if err != nil {
		return 0, nil
	}

	v, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		return 0, errs.CommonRequestParamParseFailed
	}

	return v, nil
}

func (eh CtxHandler) getParam(ctx interface{}, key string) (str string, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.Errorf("%s\n %s", e, debug.Stack())
			return
		}
	}()

	tmpResult := ""

	switch t := ctx.(type) {
	case *gin.Context:
		tv := t.Param(key)
		if tv == "" {
			return "", errs.CommonRequestParamInvalid
		}

		tmpResult = tv
	default:
		m := fmt.Sprintf("%s, type: %v", errs.FrameworkContextErrorType, t)
		panic(m)
	}

	return tmpResult, nil
}

func (eh CtxHandler) loggerRequest(ctx interface{}, payload interface{}) {
	c := ctxs.GetSelfContext(ctx)
	sessionID, ok := ctxs.GetSessionID(c)
	if !ok {
		logger.ApLog().Errorf("sessionID not found: %v, %v", tools.JsonMarshalString(ctx), tools.JsonMarshalString(payload))
		return
	}
	logger.ApLog().Debugf("Req: [sessionID: %s], payload: %s", sessionID, tools.JsonMarshalString(payload))
}

func (eh CtxHandler) loggerResponse(ctx interface{}, payload interface{}) {
	c := ctxs.GetSelfContext(ctx)
	sessionID, ok := ctxs.GetSessionID(c)

	logPayload := ""
	switch payload.(type) {
	case []byte:
		logPayload = string(payload.([]byte))
	default:
		logPayload = tools.JsonMarshalString(payload)
	}

	if !ok {
		logger.ApLog().Errorf("sessionID not found: %v, %v \n%s", tools.JsonMarshalString(ctx), logPayload, string(debug.Stack()))
		return
	}
	logger.ApLog().Debugf("Resp: [sessionID: %s], payload: %s", sessionID, logPayload)
}
