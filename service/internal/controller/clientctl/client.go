package clientctl

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/auth"
	"go.uber.org/dig"
)

type clientControllerSet struct {
	dig.In

	AuthToken auth.IToken
}

func NewController(set clientControllerSet) *Controller {
	return &Controller{
		HttpAuthToken: newAuthTokenHttpController(set.AuthToken),
		Syetem:        newSystemController(),
		Stock:         newStockController(),
	}
}

type Controller struct {
	HttpAuthToken IAuthTokenHttpController
	Syetem        ISystemController
	Stock         IStockController
}
