package clientctl

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/handler"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/auth"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/ctxs"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

func newAuthTokenHttpController(token auth.IToken) IAuthTokenHttpController {
	return &authTokenHttpController{
		token: token,
	}
}

type IAuthTokenHttpController interface {
	MiddlewareAuth(ctx *gin.Context)
}

type authTokenHttpController struct {
	token auth.IToken
}

func (at *authTokenHttpController) MiddlewareAuth(ctx *gin.Context) {
	sc := ctxs.GetSelfContext(ctx)

	token := ctx.GetHeader("Authorization")

	memberRepository, err := at.token.AuthMember(sc, token)
	if err != nil {
		handler.Ctx.Response(
			ctx,
			http.StatusUnauthorized,
			handler.ContentTypeJson,
			handler.Error.Marshal(errs.AuthTokenUnauthorized),
		)
		ctx.Abort()
		return
	}

	sc = ctxs.SetMemberID(sc, memberRepository.ID)
	sc = ctxs.SetToken(sc, memberRepository.Token)
	ctx = ctxs.SetSelfContext(sc, ctx).(*gin.Context)

	ctx.Next()
}
