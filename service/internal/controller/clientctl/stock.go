package clientctl

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func newStockController() IStockController {
	return &stockController{}
}

type IStockController interface {
	MainPage(ctx *gin.Context)
}

type stockController struct{}

func (ctr *stockController) MainPage(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "index-2.html", gin.H{})
}
