package clientctl

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/handler"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models"
)

type ISystemController interface {
	HealthCheck(ctx *gin.Context)
}

func newSystemController() ISystemController {
	return &systemController{}
}

type systemController struct {
}

func (sys *systemController) HealthCheck(ctx *gin.Context) {
	resp := &models.ResponseWithStatus{
		Status: models.CommonStatusSuccess,
	}

	if err := handler.Ctx.ResponseJsonStatusOK(ctx, resp); err != nil {
		logger.ApLog().Errorf("marshal data err: %v", err)
		handler.Ctx.ResponseJsonBadRequest(ctx, err)
		return
	}
	return
}
