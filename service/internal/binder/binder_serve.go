package binder

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/server"
	"go.uber.org/dig"
)

func provideServe(binder *dig.Container) {
	if err := binder.Provide(server.NewGinServer); err != nil {
		panic(err)
	}
}
