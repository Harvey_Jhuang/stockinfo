package binder

import "go.uber.org/dig"

var (
	binder *dig.Container
)

func New() *dig.Container {
	binder = dig.New()

	provideApp(binder)
	provideCore(binder)
	provideController(binder)
	provideServe(binder)

	return binder
}
