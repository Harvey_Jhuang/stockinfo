package binder

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/adminctl"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/clientctl"
	"go.uber.org/dig"
)

func provideController(binder *dig.Container) {
	// Controller
	if err := binder.Provide(clientctl.NewController); err != nil {
		panic(err)
	}
	if err := binder.Provide(adminctl.NewController); err != nil {
		panic(err)
	}
}
