package binder

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/auth"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/policycenter"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/stock"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/worker"
	"go.uber.org/dig"
)

func provideCore(binder *dig.Container) {
	if err := binder.Provide(auth.NewAuth); err != nil {
		panic(err)
	}

	if err := binder.Provide(policycenter.NewPolicyCenter); err != nil {
		panic(err)
	}

	if err := binder.Provide(stock.NewStock); err != nil {
		panic(err)
	}

	if err := binder.Invoke(worker.NewWorker); err != nil {
		panic(err)
	}
}
