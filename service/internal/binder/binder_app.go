package binder

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/cache"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/db"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/rbac"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/redis"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/snowflake"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/worker"
	"go.uber.org/dig"
)

func provideApp(binder *dig.Container) {
	if err := binder.Provide(snowflake.NewIDGenerator); err != nil {
		panic(err)
	}

	if err := binder.Provide(db.NewGORM); err != nil {
		panic(err)
	}

	if err := binder.Provide(redis.NewRedis); err != nil {
		panic(err)
	}

	if err := binder.Invoke(cache.NewCache); err != nil {
		panic(err)
	}

	if err := binder.Invoke(worker.New); err != nil {
		panic(err)
	}

	if err := binder.Invoke(rbac.NewAdminAuth); err != nil {
		panic(err)
	}

}
