package policycenter

import (
	"context"

	"github.com/jinzhu/gorm"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/condition"

	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

func newPolicyDAO() IPolicyDAO {
	return &policyDAO{}
}

type IPolicyDAO interface {
	Delete(dc context.Context, id string) error
	InsertOrUpdate(dc context.Context, data *model.PolicyDetail) error
	PolicyList(ctx context.Context, cond *condition.PolicyCond) ([]*model.PolicyInfo, error)
	PolicyDetail(ctx context.Context, id string) (*model.PolicyDetail, error)
	FeatureList(ctx context.Context, cond *condition.PolicyCond) ([]*model.Feature, error)
	FeatureGroupList(ctx context.Context, cond *condition.PolicyCond) ([]*model.FeatureGroup, error)
}

type policyDAO struct{}

func (dao *policyDAO) Delete(dc context.Context, id string) error {
	tx := func(dc *gorm.DB) error {
		if err := dc.Exec("DELETE FROM `access_casbin_rule` WHERE `v0` = ?", id).Error; err != nil {
			return errs.DBDeleteFailed
		}

		if err := dc.Exec("DELETE FROM `access_policy_info` WHERE `id` = ?", id).Error; err != nil {
			return errs.DBDeleteFailed
		}

		return nil
	}

	if err := packet.DB.Transaction(tx); err != nil {
		logger.ApLog().Error(err)
		return err
	}

	return nil
}

func (dao *policyDAO) InsertOrUpdate(dc context.Context, data *model.PolicyDetail) error {
	tx := func(dc *gorm.DB) error {
		if err := dc.Exec("DELETE FROM `access_casbin_rule` WHERE `v0` = ?", data.ID).Error; err != nil {
			return errs.DBDeleteFailed
		}

		if err := dc.Exec("DELETE FROM `access_policy_info` WHERE `id` = ?", data.ID).Error; err != nil {
			return errs.DBDeleteFailed
		}

		if err := dc.Exec("INSERT INTO `access_policy_info` (`id`, `name`, `description`) VALUES (?,?,?)", data.ID, data.Name, data.Description).Error; err != nil {
			return errs.DBInsertFailed
		}

		for _, features_id := range data.Features {
			var feature model.Feature
			if err := dc.Where("id = ?", features_id).First(&feature).Error; err != nil {
				return errs.DBNoRow
			}

			if err := dc.Exec("INSERT INTO `access_casbin_rule` (`p_type`,`v0`,`v1`,`v2`,`v3`,`v4`,`v5`) VALUES ('p',?,?,?,'','','') ",
				data.ID, feature.Path, feature.Method).Error; err != nil {
				return errs.DBInsertFailed
			}
		}
		return nil
	}

	if err := packet.DB.Transaction(tx); err != nil {
		logger.ApLog().Error(err)
		return err
	}

	return nil
}

func (dao *policyDAO) PolicyList(ctx context.Context, cond *condition.PolicyCond) ([]*model.PolicyInfo, error) {
	res := []*model.PolicyInfo{}

	rows, err := packet.DB.Raw("SELECT DISTINCT `v0` FROM `access_casbin_rule` WHERE `p_type` = ?", "p").Rows()
	defer rows.Close()
	if err != nil {
		return res, errs.CommonNoData
	}

	for rows.Next() {
		var (
			policy model.Policy
			info   model.PolicyInfo
		)
		err := rows.Scan(&policy.Role)
		if err != nil {
			return res, errs.CommonParseError
		}

		packet.DB.Table("access_policy_info").Select("id, name, description").Where("id = ?", policy.Role).Scan(&info)
		res = append(res, &info)
	}

	return res, nil
}

func (dao *policyDAO) FeatureList(ctx context.Context, cond *condition.PolicyCond) ([]*model.Feature, error) {
	res := []*model.Feature{}

	if err := packet.DB.Find(&res).Error; err != nil {
		return res, errs.CommonNoData
	}

	return res, nil
}

func (dao *policyDAO) PolicyDetail(ctx context.Context, id string) (*model.PolicyDetail, error) {
	var (
		info       model.PolicyInfo
		featureIDs []int32
	)

	if err := packet.DB.Table("access_policy_info").Select("id, name, description").Where("id = ?", id).Scan(&info).Error; err != nil {
		return nil, errs.CommonNoData
	}

	rows, err := packet.DB.Raw("SELECT `v1`, `v2` FROM  `access_casbin_rule` WHERE `p_type` = 'p' AND `v0` = ?", id).Rows()
	defer rows.Close()
	if err != nil {
		return nil, errs.CommonNoData
	}

	for rows.Next() {
		var cond, feature model.Feature

		rows.Scan(&cond.Path, &cond.Method)
		if err := packet.DB.Where(cond).First(&feature).Error; err != nil {
			logger.ApLog().Warn(err)
			continue
		}
		featureIDs = append(featureIDs, int32(feature.ID))
	}

	resp := &model.PolicyDetail{
		ID:          info.ID,
		Name:        info.Name,
		Description: info.Description,
		Features:    featureIDs,
	}

	return resp, nil
}

func (dao *policyDAO) FeatureGroupList(ctx context.Context, cond *condition.PolicyCond) ([]*model.FeatureGroup, error) {
	result := make([]*model.FeatureGroup, 0, 15)

	if err := packet.DB.Find(&result).Error; err != nil {
		return nil, err
	}
	return result, nil
}
