package policycenter

import (
	"context"

	"github.com/ahmetb/go-linq/v3"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/condition"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/resp"

	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/rbac"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

type IPolicy interface {
	InsertOrUpdate(ctx context.Context, data *model.PolicyDetail) (*models.ResponseWithStatus, error)
	Delete(ctx context.Context, id string) (*models.ResponseWithStatus, error)
	PolicyList(ctx context.Context, cond *models.Paging) (*models.ResponseWithData, error)
	PolicyDetail(ctx context.Context, id string) (*models.ResponseWithData, error)
	FeatureList(ctx context.Context, cond *models.Paging) (*models.ResponseWithData, error)
}

func newPolicy() IPolicy {
	return &policyUseCase{}
}

type policyUseCase struct{}

func (ju *policyUseCase) InsertOrUpdate(ctx context.Context, data *model.PolicyDetail) (*models.ResponseWithStatus, error) {
	if len(data.Features) == 0 {
		return nil, errs.CommonRequestParamInvalid
	}

	daoData := &model.PolicyDetail{
		ID:          rbac.GenerateRoleID(data.Name),
		Name:        data.Name,
		Description: data.Description,
		Features:    data.Features,
	}

	if err := dao.Policy.InsertOrUpdate(ctx, daoData); err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}
	resp := &models.ResponseWithStatus{Status: models.CommonStatusSuccess}

	ac := rbac.GetAdminAuthInstance()
	if err := ac.LoadPolicy(); err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	return resp, nil
}

func (ju *policyUseCase) Delete(ctx context.Context, id string) (*models.ResponseWithStatus, error) {
	if err := dao.Policy.Delete(ctx, id); err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}
	resp := &models.ResponseWithStatus{Status: models.CommonStatusSuccess}

	ac := rbac.GetAdminAuthInstance()
	if err := ac.LoadPolicy(); err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	return resp, nil
}

func (ju *policyUseCase) PolicyList(ctx context.Context, cond *models.Paging) (*models.ResponseWithData, error) {
	daoCond := &condition.PolicyCond{}
	res, err := dao.Policy.PolicyList(ctx, daoCond)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	resp := &models.ResponseWithData{Data: res}

	return resp, nil
}

func (ju *policyUseCase) PolicyDetail(ctx context.Context, id string) (*models.ResponseWithData, error) {
	data, err := dao.Policy.PolicyDetail(ctx, id)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	resp := &models.ResponseWithData{Data: data}
	return resp, nil
}

func (ju *policyUseCase) FeatureList(ctx context.Context, cond *models.Paging) (*models.ResponseWithData, error) {
	// 取得所有功能
	daoCond := &condition.PolicyCond{}
	features, err := dao.Policy.FeatureList(ctx, daoCond)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	// 取得所有功能類別
	featureGroups, err := dao.Policy.FeatureGroupList(ctx, daoCond)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	// 整理結果
	response := make([]*resp.GetFeatureListResp, len(featureGroups))

	for i, featureGroup := range featureGroups {
		// 取得屬於featureGroup的功能們
		fs := make([]*model.Feature, 0)
		linq.From(features).
			Where(func(i interface{}) bool {
				return i.(*model.Feature).GroupCode == featureGroup.Code
			}).
			ToSlice(&fs)

		response[i] = &resp.GetFeatureListResp{
			GroupCode: featureGroup.Code,
			GroupName: featureGroup.Name,
			Features:  fs,
		}
	}

	return &models.ResponseWithData{Data: response}, nil
}
