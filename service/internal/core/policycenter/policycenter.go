package policycenter

import (
	"sync"

	"github.com/jinzhu/gorm"
	"go.uber.org/dig"
)

var (
	packet policyCenterSet
)

var (
	once sync.Once
	ptr  *policyCenter
)

func NewPolicyCenter(set policyCenterSet) policyCenter {
	once.Do(func() {
		packet = set

		newDAO()
		ptr = &policyCenter{
			Policy: newPolicy(),
		}
	})

	return *ptr
}

type policyCenterSet struct {
	dig.In

	DB *gorm.DB
}

type policyCenter struct {
	dig.Out

	Policy IPolicy
}
