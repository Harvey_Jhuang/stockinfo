package policycenter

var (
	dao *storage
)

func newDAO() {
	dao = &storage{
		Policy: newPolicyDAO(),
	}
}

type storage struct {
	Policy IPolicyDAO
}
