package stock

import (
	"time"

	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/condition"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

type iStockJob interface {
	crawlingData(path, date string) ([]byte, error)
	extractingData(body []byte, key string) (interface{}, error)
	storingData(rawData interface{}, date time.Time) error
}

type stockJob struct {
	iStockJob iStockJob
}

func newStockJob(job iStockJob) *stockJob {
	resp := &stockJob{}
	resp.iStockJob = job
	return resp
}

func (job *stockJob) SyncData(config *condition.StockJobConfig) error {
	date, err := time.Parse("20060102", config.Date)
	if err != nil {
		logger.ApLog().Error(err)
		return errs.CommonParseError
	}

	body, err := job.iStockJob.crawlingData(config.Path, config.Date)
	if err != nil {
		logger.ApLog().Error(err)
		return err
	}

	rawData, err := job.iStockJob.extractingData(body, config.Key)
	if err != nil {
		logger.ApLog().Error(err)
		return err
	}

	if err := job.iStockJob.storingData(rawData, date); err != nil {
		logger.ApLog().Error(err)
		return err
	}

	return nil
}
