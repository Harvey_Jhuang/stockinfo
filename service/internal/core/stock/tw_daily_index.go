package stock

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/condition"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

func newDailyMarketIndexJob() *dailyMarketIndexJob {
	return &dailyMarketIndexJob{
		path:       "https://www.twse.com.tw/exchangeReport/MI_INDEX?response=json&date=%s&type=ALL&_=1598687190105",
		base:       newBasicJob(),
		keyPrice:   "data1",
		keySummary: "data7",
		keyTrend:   "data8",
	}
}

type dailyMarketIndexJob struct {
	base       *basicJob
	path       string
	keyPrice   string
	keySummary string
	keyTrend   string
}

func (job *dailyMarketIndexJob) Handler(req *condition.StockJobReq) (models.ResponseWithStatus, error) {
	resp := models.ResponseWithStatus{
		Status: models.CommonStatusFailed,
	}
	cfg := &condition.StockJobConfig{
		Path: job.path,
		Date: *req.Date,
	}

	logger.ApLog().Infof("start dailyMarketIndexJob at %s", cfg.Date)
	if err := job.SyncData(cfg); err != nil {
		logger.ApLog().Errorf("dailyMarketIndexJob at %s failed = %v", cfg.Date, err)
		return resp, err
	}

	resp.Status = models.CommonStatusSuccess

	return resp, nil
}

func (job *dailyMarketIndexJob) SyncData(config *condition.StockJobConfig) error {
	return newStockJob(job).SyncData(config)
}

func (job *dailyMarketIndexJob) crawlingData(path, date string) ([]byte, error) {
	return job.base.crawlingData(path, date)
}

func (job *dailyMarketIndexJob) extractingData(body []byte, key string) (interface{}, error) {
	var resp interface{}

	data := make(map[string]interface{})
	err := json.Unmarshal(body, &data)
	if err != nil {
		logger.ApLog().Errorf("data=%+v \n err=%v", data, err)
		return nil, err
	}

	stat, exist := data["stat"]
	if exist {
		if stat.(string) != "OK" {
			logger.ApLog().Errorf("err=%v", stat)
			return nil, errors.New(stat.(string))
		}
	}

	resp = data

	return resp, nil
}

func (job *dailyMarketIndexJob) storingData(rawData interface{}, date time.Time) error {
	data, ok := rawData.(map[string]interface{})
	if !ok {
		logger.ApLog().Errorf("data = %+v", rawData)
		return errs.CommonParseError
	}

	priceData, exist := data[job.keyPrice]
	if !exist {
		logger.ApLog().Error("key not exist")
		return errs.CommonNoData
	}

	priceDatas, ok := priceData.([]interface{})
	if !ok {
		logger.ApLog().Errorf("data = %+v", rawData)
		return errs.CommonParseError
	}

	summaryData, exist := data[job.keySummary]
	if !exist {
		logger.ApLog().Error("key not exist")
		return errs.CommonNoData
	}

	summaryDatas, ok := summaryData.([]interface{})
	if !ok {
		logger.ApLog().Errorf("data = %+v", rawData)
		return errs.CommonParseError
	}

	trendData, exist := data[job.keyTrend]
	if !exist {
		logger.ApLog().Error("key not exist")
		return errs.CommonNoData
	}

	trendDatas, ok := trendData.([]interface{})
	if !ok {
		logger.ApLog().Errorf("data = %+v", rawData)
		return errs.CommonParseError
	}

	tx := func(dc *gorm.DB) error {
		market, ok := priceDatas[1].([]interface{})
		if !ok {
			logger.ApLog().Errorf("priceData= %+v is not array", priceDatas[1])
			return errs.CommonParseError
		}

		summary, ok := summaryDatas[14].([]interface{})
		if !ok {
			logger.ApLog().Errorf("summaryData= %+v is not array", summaryDatas[14])
			return errs.CommonParseError
		}

		data, err := job.arrangeData(market, summary, trendDatas, date)
		if err != nil {
			logger.ApLog().Error(err)
			return err
		}

		if err := dao.TWStock.Create(dc, data); err != nil {
			logger.ApLog().Error(err)
			return errs.DBInsertFailed
		}
		return nil
	}

	if err := packet.DB.Transaction(tx); err != nil {
		logger.ApLog().Error(err)
		return err
	}

	return nil
}

func (job *dailyMarketIndexJob) arrangeData(market, summary, trends []interface{}, date time.Time) (*model.MarketIndex, error) {

	closePrice, err := strconv.ParseFloat(strings.ReplaceAll(market[1].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		closePrice = -1.0
	}

	trend := removeTag.ReplaceAllString(market[2].(string), "")

	priceDiff, err := strconv.ParseFloat(strings.ReplaceAll(market[3].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		priceDiff = -1.0
	}

	priceDiffRatio, err := strconv.ParseFloat(strings.ReplaceAll(market[4].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		priceDiffRatio = -1.0
	}

	remarks, ok := market[5].(string)
	if !ok {
		remarks = ""
	}

	amountOfDeal, err := strconv.ParseFloat(strings.ReplaceAll(summary[1].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		amountOfDeal = -1.0
	}

	numberOfShare, err := strconv.ParseInt(strings.ReplaceAll(summary[2].(string), ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfShare = -1
	}

	numberOfDeal, err := strconv.ParseInt(strings.ReplaceAll(summary[3].(string), ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfDeal = -1
	}

	ups := getDigitTag.FindAllString(trends[0].([]interface{})[2].(string), -1)
	numberOfStockLimitUp, err := strconv.ParseInt(strings.ReplaceAll(ups[1], ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfStockLimitUp = -1
	}

	numberOfStockGoUp, err := strconv.ParseInt(strings.ReplaceAll(ups[0], ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfStockGoUp = -1
	}

	downs := getDigitTag.FindAllString(trends[1].([]interface{})[2].(string), -1)
	numberOfStockLimitDown, err := strconv.ParseInt(strings.ReplaceAll(downs[1], ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfStockLimitDown = -1
	}

	numberOfStockGoDown, err := strconv.ParseInt(strings.ReplaceAll(downs[0], ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfStockGoDown = -1
	}

	numberOfStockUnchanged, err := strconv.ParseInt(strings.ReplaceAll(trends[2].([]interface{})[2].(string), ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfStockUnchanged = -1
	}

	index := &model.MarketIndex{
		Name:                   market[0].(string),
		Date:                   date,
		Trend:                  trend,
		ClosePrice:             closePrice,
		PriceDiff:              priceDiff,
		PriceDiffRatio:         priceDiffRatio,
		NumberOfShare:          numberOfShare,
		NumberOfDeal:           numberOfDeal,
		AmountOfDeal:           amountOfDeal,
		NumberOfStockLimitUp:   numberOfStockLimitUp,
		NumberOfStockLimitDown: numberOfStockLimitDown,
		NumberOfStockGoUp:      numberOfStockGoUp,
		NumberOfStockGoDown:    numberOfStockGoDown,
		NumberOfStockUnchanged: numberOfStockUnchanged,
		Remarks:                remarks,
	}

	return index, nil
}
