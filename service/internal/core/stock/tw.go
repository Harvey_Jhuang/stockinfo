package stock

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/worker"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/condition"
)

var token ITWStock

func newTWStock() ITWStock {
	return &twStock{
		closeInfo:   newDailyCloseInfoJob(),
		marketIndex: newDailyMarketIndexJob(),
	}
}

type ITWStock interface {
	SyncDailyCloseInfo(period string)

	ExecDailyCloseInfo(req *condition.StockJobReq) (models.ResponseWithStatus, error)
	ExecDailyMarketIndex(req *condition.StockJobReq) (models.ResponseWithStatus, error)
}

type twStock struct {
	closeInfo   *dailyCloseInfoJob
	marketIndex *dailyMarketIndexJob
}

func (tk *twStock) ExecDailyCloseInfo(req *condition.StockJobReq) (models.ResponseWithStatus, error) {
	return tk.closeInfo.Handler(req)
}

func (tk *twStock) ExecDailyMarketIndex(req *condition.StockJobReq) (models.ResponseWithStatus, error) {
	return tk.marketIndex.Handler(req)
}

func (tk *twStock) SyncDailyCloseInfo(period string) {
	worker.Cron().AddFunc(period, tk.closeInfo.SyncJob)
}
