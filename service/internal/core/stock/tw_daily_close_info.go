package stock

import (
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/condition"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

func newDailyCloseInfoJob() *dailyCloseInfoJob {
	return &dailyCloseInfoJob{
		path: "https://www.twse.com.tw/exchangeReport/MI_INDEX?response=json&date=%s&type=ALL&_=1598687190105",
		base: newBasicJob(),
		key:  "data9",
	}
}

type dailyCloseInfoJob struct {
	base *basicJob
	path string
	key  string
}

func (job *dailyCloseInfoJob) Handler(req *condition.StockJobReq) (models.ResponseWithStatus, error) {
	resp := models.ResponseWithStatus{
		Status: models.CommonStatusFailed,
	}
	cfg := &condition.StockJobConfig{
		Path: job.path,
		Date: *req.Date,
		Key:  job.key,
	}

	logger.ApLog().Infof("start dailyCloseInfoJob at %s", cfg.Date)
	if err := job.SyncData(cfg); err != nil {
		logger.ApLog().Errorf("dailyCloseInfoJob at %s failed = %v", cfg.Date, err)
		return resp, err
	}

	resp.Status = models.CommonStatusSuccess

	return resp, nil
}

func (job *dailyCloseInfoJob) SyncJob() {
	date := time.Now().Format("20060102")
	cfg := &condition.StockJobConfig{
		Path: job.path,
		Date: date,
		Key:  job.key,
	}

	logger.ApLog().Infof("start dailyCloseInfoJob at %s", cfg.Date)
	if err := job.SyncData(cfg); err != nil {
		logger.ApLog().Error(err)
	}
}

func (job *dailyCloseInfoJob) SyncData(config *condition.StockJobConfig) error {
	return newStockJob(job).SyncData(config)
}

func (job *dailyCloseInfoJob) crawlingData(path, date string) ([]byte, error) {
	return job.base.crawlingData(path, date)
}

func (job *dailyCloseInfoJob) extractingData(body []byte, key string) (interface{}, error) {
	return job.base.extractingData(body, key)
}

func (job *dailyCloseInfoJob) storingData(rawData interface{}, date time.Time) error {
	data, ok := rawData.([]interface{})
	if !ok {
		logger.ApLog().Errorf("data = %+v", rawData)
		return errs.CommonParseError
	}

	tx := func(dc *gorm.DB) error {
		for _, stock := range data {
			stockData, ok := stock.([]interface{})
			if !ok {
				logger.ApLog().Errorf("stockData = %+v", stockData)
				return errs.CommonParseError
			}

			info, err := job.arrangeData(stockData, date)
			if err != nil {
				logger.ApLog().Error(err)
				return err
			}

			if err := dao.TWStock.Create(dc, info); err != nil {
				logger.ApLog().Error(err)
				return errs.DBInsertFailed
			}
		}
		return nil
	}

	if err := packet.DB.Transaction(tx); err != nil {
		logger.ApLog().Error(err)
		return err
	}

	return nil
}

func (job *dailyCloseInfoJob) arrangeData(stock []interface{}, date time.Time) (*model.StockCloseInfo, error) {
	numberOfShare, err := strconv.ParseInt(strings.ReplaceAll(stock[2].(string), ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfShare = -1
	}

	numberOfDeal, err := strconv.ParseInt(strings.ReplaceAll(stock[3].(string), ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfDeal = -1
	}

	amountOfDeal, err := strconv.ParseFloat(strings.ReplaceAll(stock[4].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		amountOfDeal = -1.0
	}

	openPrice, err := strconv.ParseFloat(strings.ReplaceAll(stock[5].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		openPrice = -1.0
	}

	highPrice, err := strconv.ParseFloat(strings.ReplaceAll(stock[6].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		highPrice = -1.0
	}

	lowPrice, err := strconv.ParseFloat(strings.ReplaceAll(stock[7].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		lowPrice = -1.0
	}

	closePrice, err := strconv.ParseFloat(strings.ReplaceAll(stock[8].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		closePrice = -1.0
	}

	trend := removeTag.ReplaceAllString(stock[9].(string), "")
	priceDiff, err := strconv.ParseFloat(strings.ReplaceAll(stock[10].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		priceDiff = -1.0
	}

	lastAsk, err := strconv.ParseFloat(strings.ReplaceAll(stock[11].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		lastAsk = -1.0
	}

	numberOfLastAsk, err := strconv.ParseInt(strings.ReplaceAll(stock[12].(string), ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfLastAsk = -1
	}

	lastBid, err := strconv.ParseFloat(strings.ReplaceAll(stock[13].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		lastBid = -1.0
	}

	numberOfLastBid, err := strconv.ParseInt(strings.ReplaceAll(stock[14].(string), ",", ""), 10, 64)
	if err != nil {
		logger.ApLog().Warn(err)
		numberOfLastBid = -1
	}

	peRatio, err := strconv.ParseFloat(strings.ReplaceAll(stock[15].(string), ",", ""), 64)
	if err != nil {
		logger.ApLog().Warn(err)
		peRatio = -1.0
	}

	info := &model.StockCloseInfo{
		Code:            stock[0].(string),
		Date:            date,
		Name:            stock[1].(string),
		NumberOfShare:   numberOfShare,
		NumberOfDeal:    numberOfDeal,
		AmountOfDeal:    amountOfDeal,
		OpenPrice:       openPrice,
		HighPrice:       highPrice,
		LowPrice:        lowPrice,
		ClosePrice:      closePrice,
		Trend:           trend,
		PriceDiff:       priceDiff,
		LastAsk:         lastAsk,
		NumberOfLastAsk: numberOfLastAsk,
		LastBid:         lastBid,
		NumberOfLastBid: numberOfLastBid,
		PERatio:         peRatio,
	}

	return info, nil
}
