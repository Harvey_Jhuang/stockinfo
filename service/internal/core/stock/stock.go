package stock

import (
	"regexp"
	"sync"

	"github.com/go-redis/redis/v7"
	"github.com/jinzhu/gorm"
	"go.uber.org/dig"
)

var (
	packet      stockSet
	removeTag   = regexp.MustCompile(`<[^>]*>`)
	getDigitTag = regexp.MustCompile(`\d+`)
)

var (
	once sync.Once
	self *stock
)

func NewStock(set stockSet) stockOut {
	once.Do(func() {
		packet = set
		newDAO()

		self = &stock{
			stockOut: stockOut{
				TWStock: newTWStock(),
			},
		}

	})

	return self.stockOut
}

type stockSet struct {
	dig.In

	DB    *gorm.DB
	Redis *redis.Client
}

type stock struct {
	stockOut
}

type stockOut struct {
	dig.Out

	TWStock ITWStock
}
