package stock

var (
	dao *storage
)

func newDAO() {
	dao = &storage{
		TWStock: newTWStockDAO(),
	}
}

type storage struct {
	TWStock ITWStockDAO
}
