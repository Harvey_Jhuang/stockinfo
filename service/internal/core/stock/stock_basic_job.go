package stock

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

func newBasicJob() *basicJob {
	return &basicJob{}
}

type basicJob struct{}

func (job *basicJob) crawlingData(path, date string) ([]byte, error) {
	url := fmt.Sprintf(path, date)

	logger.ApLog().Infof("job.iStockJob.crawlingData for url=%s", url)

	userAgent := "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36"

	cli := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	req.Header.Set("User-Agent", userAgent)

	resp, err := cli.Do(req)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, err
	}

	return body, nil
}

func (job *basicJob) extractingData(body []byte, key string) (interface{}, error) {
	data := make(map[string]interface{})
	err := json.Unmarshal(body, &data)
	if err != nil {
		logger.ApLog().Errorf("data=%+v \n err=%v", data, err)
		return nil, err
	}

	stat, exist := data["stat"]
	if exist {
		if stat.(string) != "OK" {
			logger.ApLog().Errorf("err=%v", stat)
			return nil, errors.New(stat.(string))
		}
	}

	rawData, exist := data[key]
	if !exist {
		logger.ApLog().Error("key not exist")
		return nil, errs.CommonNoData
	}

	return rawData, nil
}

func (job *basicJob) storingData(rawData interface{}, date time.Time) error {
	return errs.CommonInterfaceNotImplement
}
