package stock

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

func newTWStockDAO() ITWStockDAO {
	return &twStockDAO{}
}

type ITWStockDAO interface {
	Create(dc *gorm.DB, data interface{}) error
}

type twStockDAO struct {
	model.StockCloseInfo
}

func (dao *twStockDAO) Create(dc *gorm.DB, data interface{}) error {
	if err := dc.Create(data).Error; err != nil {
		return errs.ConvertDB(err)
	}

	return nil
}
