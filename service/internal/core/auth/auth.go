package auth

import (
	"sync"

	"github.com/go-redis/redis/v7"
	"github.com/jinzhu/gorm"
	"go.uber.org/dig"
)

var (
	packet authSet
)

var (
	once sync.Once
	self *auth
)

func NewAuth(set authSet) authOut {
	once.Do(func() {
		packet = set

		self = &auth{
			authOut: authOut{
				Token: newToken(),
			},
		}

	})

	return self.authOut
}

type authSet struct {
	dig.In

	DB    *gorm.DB
	Redis *redis.Client
}

type auth struct {
	authOut
}

type authOut struct {
	dig.Out

	Token IToken
}
