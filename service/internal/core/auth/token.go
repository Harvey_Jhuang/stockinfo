package auth

import (
	"context"
	"strconv"

	"github.com/google/uuid"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/conf"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

var token IToken

func newToken() IToken {
	token = &tokenUseCase{}
	return token
}

type IToken interface {
	Gen(ctx context.Context, id int64) (string, error)
	DeleteByToken(ctx context.Context, token string) error
	DeleteByID(ctx context.Context, id int64) error

	AuthMember(ctx context.Context, token string) (*model.AuthToken, error)
	GetMemberByID(id int64) (*model.AuthToken, bool)
	GetMemberByToken(token string) (*model.AuthToken, bool)

	AuthAdmin(ctx context.Context, token string) (*model.AuthToken, error)
}

type tokenUseCase struct{}

func (tk *tokenUseCase) Gen(ctx context.Context, id int64) (string, error) {

	token := tk.genToken()
	idStr := strconv.FormatInt(id, 10)

	if err := packet.Redis.Set(token, idStr, conf.LoginMember().GetTokenExpiration()).Err(); err != nil {
		logger.ApLog().Error(err)
		return "", errs.AuthTokenCreateFailed
	}

	if err := packet.Redis.Set(idStr, token, conf.LoginMember().GetTokenExpiration()).Err(); err != nil {
		logger.ApLog().Error(err)
		return "", errs.AuthTokenCreateFailed
	}

	return token, nil
}

// gen token
func (tk *tokenUseCase) genToken() string {
	return uuid.New().String()
}

func (tk *tokenUseCase) DeleteByToken(ctx context.Context, token string) error {
	idStr, err := packet.Redis.Get(token).Result()
	if err != nil {
		logger.ApLog().Infof("token:[%s] err:[%v]", token, err)
	}

	if err := packet.Redis.Del(idStr).Err(); err != nil {
		logger.ApLog().Warnf("idStr:[%s] token:[%s] err:[%v]", idStr, token, err)
	}

	if err := packet.Redis.Del(token).Err(); err != nil {
		logger.ApLog().Warnf("idStr:[%s] token:[%s] err:[%v]", idStr, token, err)
	}

	return nil
}

func (tk *tokenUseCase) DeleteByID(ctx context.Context, id int64) error {
	idStr := strconv.FormatInt(id, 10)

	token, err := packet.Redis.Get(idStr).Result()
	if err != nil {
		logger.ApLog().Infof("idStr:[%s] err:[%v]", idStr, err)
	}

	if err := packet.Redis.Del(idStr).Err(); err != nil {
		logger.ApLog().Warnf("idStr:[%s] token:[%s] err:[%v]", idStr, token, err)
	}

	if err := packet.Redis.Del(token).Err(); err != nil {
		logger.ApLog().Warnf("idStr:[%s] token:[%s] err:[%v]", idStr, token, err)
	}

	return nil
}

func (tk *tokenUseCase) auth(ctx context.Context, token string) (int64, error) {
	idStr, err := packet.Redis.Get(token).Result()
	if err != nil {
		logger.ApLog().Errorf("token:[%s] err:[%v]", token, err)
		return 0, errs.AuthTokenUnauthorized
	}

	if err = packet.Redis.Set(token, idStr, conf.LoginMember().GetTokenExpiration()).Err(); err != nil {
		logger.ApLog().Errorf("idStr:[%s] token:[%s] err:[%v]", idStr, token, err)
		return 0, errs.AuthTokenCreateFailed
	}

	if err = packet.Redis.Set(idStr, token, conf.LoginMember().GetTokenExpiration()).Err(); err != nil {
		logger.ApLog().Errorf("idStr:[%s] token:[%s] err:[%v]", idStr, token, err)
		return 0, errs.AuthTokenCreateFailed
	}

	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		logger.ApLog().Errorf("idStr:[%s] token:[%s] err:[%v]", idStr, token, err)
		return 0, errs.AuthTokenUnauthorized
	}

	return id, nil
}

func (tk *tokenUseCase) AuthMember(ctx context.Context, token string) (*model.AuthToken, error) {
	id, err := tk.auth(ctx, token)
	if err != nil {
		logger.ApLog().Errorf("token:%s err:%v", token, err)
		return nil, errs.AuthTokenUnauthorized
	}

	return &model.AuthToken{ID: id, Token: token}, nil
}

func (tk *tokenUseCase) GetMemberByID(id int64) (*model.AuthToken, bool) {
	var (
		result *model.AuthToken
		exist  bool
	)

	idStr := strconv.FormatInt(id, 10)

	token, err := packet.Redis.Get(idStr).Result()
	if err != nil {
		return result, exist
	}

	result = &model.AuthToken{
		ID:    id,
		Token: token,
	}

	exist = true

	return result, exist
}

func (tk *tokenUseCase) GetMemberByToken(token string) (*model.AuthToken, bool) {
	var (
		result *model.AuthToken
		exist  bool
	)

	idStr, err := packet.Redis.Get(token).Result()
	if err != nil {
		return result, exist
	}

	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return result, exist
	}

	result = &model.AuthToken{
		ID:    id,
		Token: token,
	}

	exist = true

	return result, exist
}

func (tk *tokenUseCase) AuthAdmin(ctx context.Context, token string) (*model.AuthToken, error) {
	id, err := tk.auth(ctx, token)
	if err != nil {
		logger.ApLog().Error(err)
		return nil, errs.AuthTokenUnauthorized
	}

	adminRepository := &model.AuthToken{
		Token: token,
		ID:    id,
	}

	return adminRepository, nil
}
