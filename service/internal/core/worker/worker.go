package worker

import (
	"sync"

	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/core/stock"
	"go.uber.org/dig"
)

var (
	once   sync.Once
	packet workerSet
	self   *worker
)

func NewWorker(set workerSet) {
	once.Do(func() {
		packet = set

		//logger.ApLog().Infof("SyncDailyCloseInfo rule = %s", conf.Worker().GetTWStockCloseInfo())
		//packet.TWStock.SyncDailyCloseInfo(conf.Worker().GetTWStockCloseInfo())

		self = &worker{}
	})
}

type workerSet struct {
	dig.In

	TWStock stock.ITWStock
}

type worker struct {
}
