package worker

import (
	"github.com/robfig/cron/v3"
)

var c *cron.Cron

func Cron() *cron.Cron {
	return c
}

func New() {
	c = cron.New()
}
