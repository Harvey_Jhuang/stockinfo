package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/conf"
)

const (
	GormQueryOption = "gorm:query_option"
)

const (
	SyntaxForUpdate = "FOR UPDATE"
)

var (
	GormSetSelectForUpdate = func() (string, string) { return GormQueryOption, SyntaxForUpdate }
)

func NewGORM() *gorm.DB {
	db, err := gorm.Open("sqlite3", "./stockinfo.db")
	if err != nil {
		logger.ApLog().Panic(err)
	}

	db.DB().SetMaxIdleConns(2)
	db.DB().SetMaxOpenConns(100)
	db.LogMode(conf.DB().GetGormLogMode())

	migrate(db)

	return db
}

func migrate(db *gorm.DB) {
	logger.ApLog().Info("start migrate")
	db.AutoMigrate(&model.Feature{})
	db.AutoMigrate(&model.PolicyInfo{})
	db.AutoMigrate(&model.StockCloseInfo{})
	db.AutoMigrate(&model.MarketIndex{})
}
