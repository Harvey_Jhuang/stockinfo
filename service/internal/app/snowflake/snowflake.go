package snowflake

import (
	"github.com/bwmarrin/snowflake"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
)

func NewIDGenerator() *snowflake.Node {
	snowflake.Epoch = 1580638252000
	node, err := snowflake.NewNode(1)
	if err != nil {
		logger.ApLog().Error(err)
		panic(err)
	}
	return node
}
