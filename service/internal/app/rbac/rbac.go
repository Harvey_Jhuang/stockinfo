package rbac

import (
	"crypto/md5"
	"encoding/hex"
	"os"

	"github.com/casbin/casbin/v2"
	gormadapter "github.com/casbin/gorm-adapter/v2"
	"github.com/jinzhu/gorm"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"
)

var (
	adminInstance *AdminAuth
	adminPrefix   = "access"
)

type AdminAuth struct {
	*casbin.Enforcer
}

func NewAdminAuth(db *gorm.DB) {
	dir, err := os.Getwd()
	if err != nil {
		logger.ApLog().Panic(err)
	}
	logger.ApLog().Info(dir)

	cas, err := casbin.NewEnforcer("conf.d/rbac_model.conf")
	if err != nil {
		logger.ApLog().Panic(err)
	}

	adapter, err := gormadapter.NewAdapterByDBUsePrefix(db, adminPrefix+"_")
	if err != nil {
		logger.ApLog().Panic(err)
	}
	cas.SetAdapter(adapter)
	cas.EnableAutoSave(true)

	adminInstance = &AdminAuth{
		cas,
	}

	err = adminInstance.adminInitData(db)
	if err != nil {
		logger.ApLog().Panic(err)
	}
}

func GetAdminAuthInstance() *AdminAuth {
	return adminInstance
}

func GenerateRoleID(name string) string {
	hash := md5.Sum([]byte(name))
	return hex.EncodeToString(hash[:])
}

func (ac *AdminAuth) adminInitData(db *gorm.DB) error {

	var features []*model.Feature

	_ = ac.Enforcer.LoadPolicy()
	id := GenerateRoleID("admin")

	if err := db.Find(&features).Error; err != nil {
		return err
	}

	// for admin
	for _, value := range features {
		ac.AddPolicy(id, value.Path, value.Method)
	}

	info := &model.PolicyInfo{
		ID:          id,
		Name:        "admin",
		Description: "default",
	}
	if err := db.Where("id = ?", id).Assign(info).FirstOrCreate(&info).Error; err != nil {
		return err
	}

	ac.AddRoleForUser("3404368246210561", id)

	return nil
}
