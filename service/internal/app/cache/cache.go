package cache

import (
	"time"

	"github.com/patrickmn/go-cache"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/conf"
)

const (
	KeyDefault = "default"
)

var c *cache.Cache

func Cache() *cache.Cache {
	return c
}

func NewCache() {
	c = newCache(conf.Cache().GetDefaultExpiration())
}

func newCache(defaultExpiration time.Duration) *cache.Cache {
	return cache.New(defaultExpiration, time.Second)
}
