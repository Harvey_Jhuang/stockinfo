package conf

import (
	"github.com/spf13/viper"
)

type WorkerHandler struct {
}

func (WorkerHandler) GetTWStockCloseInfo() string {
	return viper.GetString("worker.twStockCloseInfoJob")
}
