package conf

import "github.com/spf13/viper"

type DBHandler struct{}

// 預設過期秒數
func (DBHandler) GetRawSQLRoot() string {
	return viper.GetString("db.raw_sql_root")
}

func (DBHandler) GetRawSQLUrl() string {
	return viper.GetString("db.sql_url")
}

func (DBHandler) GetGormLogMode() bool {
	return viper.GetBool("db.gorm_log_mode")
}
