package conf

import "github.com/spf13/viper"

type IService interface {
	GetApisWsPort() string
	GetApisHttpPort() string
	GetAdminGRPCPort() string
	GetCustomerPort() string
}

type ServiceHandler struct{}

// apis ws port
func (ServiceHandler) GetApisWsPort() string {
	return ":" + viper.GetString("service.port.apis_ws")
}

// apis http port
func (ServiceHandler) GetApisHttpPort() string {
	return viper.GetString("service.port.apis_http")
}

// grpc port
func (ServiceHandler) GetAdminGRPCPort() string {
	return ":" + viper.GetString("service.port.admin_grpc")
}

// auto port
func (ServiceHandler) GetAutoWsPort() string {
	return ":" + viper.GetString("service.port.auto_ws")
}

// customer port
func (ServiceHandler) GetCustomerPort() string {
	return ":" + viper.GetString("service.port.customer_http")
}
