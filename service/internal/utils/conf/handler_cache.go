package conf

import (
	"github.com/spf13/viper"
	"time"
)

type CacheHandler struct{}

// 預設過期秒數
func (CacheHandler) GetDefaultExpiration() time.Duration {
	return viper.GetDuration("cache.default_expiration") * time.Second
}
