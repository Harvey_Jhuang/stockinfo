package conf

import "github.com/spf13/viper"

type LogHandler struct{}

// 系統log
func (LogHandler) GetSysLog() string {
	return viper.GetString("log.sys_log")
}

// 服務log
func (LogHandler) GetApLog() string {
	return viper.GetString("log.ap_log")
}
