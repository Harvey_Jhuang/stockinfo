package conf

import "github.com/spf13/viper"

type RedisHandler struct{}

func (RedisHandler) GetHost() string {
	return viper.GetString("redis.host")
}

func (RedisHandler) GetPasswd() string {
	return viper.GetString("redis.passwd")
}

func (RedisHandler) GetDB() int {
	return viper.GetInt("redis.db")
}
