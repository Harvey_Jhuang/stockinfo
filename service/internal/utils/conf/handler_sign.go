package conf

import "github.com/spf13/viper"

type SignHandler struct{}

// 預設過期秒數
func (SignHandler) GetSalt() []byte {
	return []byte(viper.GetString("sign.salt"))
}
