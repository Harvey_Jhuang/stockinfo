package conf

import (
	"time"

	"github.com/spf13/viper"
)

type LoginMemberHandler struct{}

func (LoginMemberHandler) GetMode() int {
	return viper.GetInt("auth.mode")
}

// 驗證碼:字集
func (LoginMemberHandler) GetVerificationCodeDictionary() string {
	return viper.GetString("auth.login_member.verification_code_dictionary")
}

// 驗證碼:產生幾碼
func (LoginMemberHandler) GetVerificationCodeDigit() int {
	return viper.GetInt("auth.login_member.verification_code_digit")
}

// 驗證碼有效秒數
func (LoginMemberHandler) GetVerificationCodeExpiration() time.Duration {
	return viper.GetDuration("auth.login_member.verification_code_expiration") * time.Second
}

// 重發驗證碼延遲秒數(幾秒後能重發)
func (LoginMemberHandler) GetVerificationCodeResendDelayTime() time.Duration {
	return viper.GetDuration("auth.login_member.verification_code_resend_delay_time") * time.Second
}

// 驗證失敗計次重置時間
func (LoginMemberHandler) GetVerificationFailedCountResetTime() time.Duration {
	return viper.GetDuration("auth.login_member.verification_failed_count_reset_time") * time.Second
}

// 驗證失敗限制
func (LoginMemberHandler) GetVerificationFailedLimit() int {
	return viper.GetInt("auth.login_member.verification_failed_limit")
}

// 重發反轉驗證碼延遲秒數(幾秒後能重發)
func (LoginMemberHandler) GetReverseVerificationCodeResendDelayTime() time.Duration {
	return viper.GetDuration("auth.login_member.reverse_verification_code_resend_delay_time") * time.Second
}

// 反轉驗證失敗計次重置時間
func (LoginMemberHandler) GetReverseVerificationFailedCountResetTime() time.Duration {
	return viper.GetDuration("auth.login_member.reverse_verification_failed_count_reset_time") * time.Second
}

// 反轉驗證失敗限制
func (LoginMemberHandler) GetReverseVerificationFailedLimit() int {
	return viper.GetInt("auth.login_member.reverse_verification_failed_limit")
}

// token expiration ? min
func (LoginMemberHandler) GetTokenExpiration() time.Duration {
	return viper.GetDuration("auth.login_member.token_expiration") * time.Minute
}
