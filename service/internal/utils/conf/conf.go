package conf

import (
	"encoding/json"
	"flag"
	"log"
	"path/filepath"

	"github.com/spf13/viper"
)

type EnvType = string

const (
	EnvTypeLocal EnvType = "local"
	EnvTypeDev   EnvType = "dev"
	EnvTypeProd  EnvType = "prod"

	defaultEnv EnvType = EnvTypeLocal
)

var (
	zqbConf *ZQBConf
	appConf *AppConf
)

func Env() string {
	return zqbConf.properties.env
}

func Service() ServiceHandler {
	return zqbConf.properties.zqbYaml.Service
}

func DB() DBHandler {
	return appConf.properties.appYaml.DB
}

func Sign() SignHandler {
	return zqbConf.properties.zqbYaml.Sign
}

func Log() LogHandler {
	return zqbConf.properties.zqbYaml.Log
}

func Cache() CacheHandler {
	return zqbConf.properties.zqbYaml.Cache
}

func LoginMember() LoginMemberHandler {
	return zqbConf.properties.zqbYaml.Auth.LoginMember
}

func Worker() WorkerHandler {
	return zqbConf.properties.zqbYaml.Worker
}

func Redis() RedisHandler {
	return appConf.properties.appYaml.Redis
}

func Start() {
	objZQB := &ZQBConf{
		properties: &ZQBProperties{},
	}
	objZQB.Load()

	zqbConf = objZQB

	objApp := &AppConf{
		properties: &AppProperties{},
	}
	objApp.Load()

	appConf = objApp
}

type ZQBConf struct {
	properties *ZQBProperties
}

func (c *ZQBConf) Load() {
	c.loadEnv()
	c.loadYaml()
}

func (c *ZQBConf) loadEnv() {
	envArg := flag.String("env", defaultEnv, "the server run which environment")
	flag.Parse()

	env := ""
	switch e := *envArg; e {
	case EnvTypeLocal, EnvTypeDev, EnvTypeProd:
		env = e
	default:
		env = defaultEnv
	}

	c.properties.env = env
	log.Printf("Env: %s\n", env)
}

func (c *ZQBConf) loadYaml() {
	path, err := filepath.Abs("conf.d/zqb.yaml")
	if err != nil {
		panic(err)
	}
	viper.SetConfigFile(path)

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	viper.AllSettings()

	if c.properties.env == EnvTypeDev {
		val := viper.AllSettings()
		if buf, err := json.Marshal(&val); err == nil {
			log.Printf("zqbYaml: %s", string(buf))
		}
	}
}

type AppConf struct {
	properties *AppProperties
}

func (c *AppConf) Load() {
	c.loadYaml()
}

func (c *AppConf) loadYaml() {
	path, err := filepath.Abs("conf.d")
	if err != nil {
		panic(err)
	}
	viper.SetConfigName("app.conf")
	viper.SetConfigType("yaml") // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(path)
	viper.MergeInConfig()
}

type ZQBProperties struct {
	zqbYaml ZqbYaml
	env     string
}

type AppProperties struct {
	appYaml AppYaml
}

type ZqbYaml struct {
	Service ServiceHandler `yaml:"service"`
	Sign    SignHandler    `yaml:"sign"`
	Log     LogHandler     `yaml:"log"`
	Cache   CacheHandler   `yaml:"cache"`
	Auth    AuthHandler    `yaml:"auth"`
	Worker  WorkerHandler  `yaml:"worker"`
}

type AppYaml struct {
	DB    DBHandler    `yaml:"db"`
	Redis RedisHandler `yaml:"redis"`
}
