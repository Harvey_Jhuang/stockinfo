package errs

import "net/http"

// ErrorCode: httpStatus-code
// ex: 400-1
type ErrorCode string

// httpStatus reference to HTTP Status Code
type httpStatus int

const (
	statusBadRequest   httpStatus = http.StatusBadRequest
	statusUnauthorized httpStatus = http.StatusUnauthorized
	statusForbidden    httpStatus = http.StatusForbidden
)

// prefix code
type prefixCode int

const (
	codeCommon prefixCode = 0
	codeAuth   prefixCode = 1
	codePolicy prefixCode = 2
)

// common error code
var (
	DBNoRow           = new(statusBadRequest, codeCommon, 1, "资料不存在")
	DBUpdateFailed    = new(statusBadRequest, codeCommon, 2, "资料修改失败")
	DBUpdateDuplicate = new(statusBadRequest, codeCommon, 3, "资料修改后未异动")
	DBInsertFailed    = new(statusBadRequest, codeCommon, 4, "资料新增失败")
	DBInsertDuplicate = new(statusBadRequest, codeCommon, 5, "资料重复新增")
	DBFetchFailed     = new(statusBadRequest, codeCommon, 6, "资料取得失败")
	DBDeleteFailed    = new(statusBadRequest, codeCommon, 7, "资料删除失败")
	DBAccessDenied    = new(statusBadRequest, codeCommon, 8, "资料访问被拒绝")
	DBOperationFailed = new(statusBadRequest, codeCommon, 9, "资料操作失败")
	DBUpdateZeroRow   = new(statusBadRequest, codeCommon, 10, "资料更新比数为0")
)

var (
	CommonUnknownError            = new(statusBadRequest, codeCommon, 101, "未知错误")
	CommonNoData                  = new(statusBadRequest, codeCommon, 102, "查无资料")
	CommonRawSQLNotFound          = new(statusBadRequest, codeCommon, 103, "找不到执行档")
	CommonNoMemberID              = new(statusBadRequest, codeCommon, 104, "无法取得会员ID")
	CommonRequestParamInvalid     = new(statusBadRequest, codeCommon, 105, "请求参数错误")
	CommonRequestParamParseFailed = new(statusBadRequest, codeCommon, 106, "请求参数解析失败")
	CommonRequestPageError        = new(statusBadRequest, codeCommon, 107, "请求的页数错误")
	CommonParseError              = new(statusBadRequest, codeCommon, 108, "解析失败")
	CommonNoAdminID               = new(statusBadRequest, codeCommon, 109, "无法取得管端ID")
	CommonRecordOwnershipWrong    = new(statusBadRequest, codeCommon, 110, "非该会员资料")
	CommonInterfaceNotImplement   = new(statusBadRequest, codeCommon, 111, "接口未被實作")
)

var (
	FrameworkContextErrorType      = new(statusBadRequest, codeCommon, 201, "Context 型别错误")
	FrameworkNotProtoBuf           = new(statusBadRequest, codeCommon, 202, "型别不是protoBuf")
	FrameworkRequestBodyParseError = new(statusBadRequest, codeCommon, 203, "请求解析失败")
	FrameworkIllegalParameter      = new(statusBadRequest, codeCommon, 204, "请求参数错误")
)

var (
	FileServerUploadFailed  = new(statusBadRequest, codeCommon, 301, "图片上传失败")
	FileServerResponseNotOK = new(statusBadRequest, codeCommon, 302, "图片库异常")
)

var (
	WsReadFailed   = new(statusBadRequest, codeCommon, 401, "无法读取讯息")
	WsTopicError   = new(statusBadRequest, codeCommon, 402, "频道错误")
	WsParseError   = new(statusBadRequest, codeCommon, 403, "解析错误")
	WsReadTimeout  = new(statusBadRequest, codeCommon, 404, "读取超时")
	WsStatusFailed = new(statusBadRequest, codeCommon, 405, "回应状态码为失败")
)

var (
	BufSpaceFilled = new(statusBadRequest, codeCommon, 501, "缓冲空间已满")
)

var (
	MongoDBZeroInsertCount = new(statusBadRequest, codeCommon, 601, "mongo未写入资料")
	MongoDBInsertFailed    = new(statusBadRequest, codeCommon, 602, "mongo资料新增失败")
)

var (
	RegistrationMobileFmtErr       = new(statusBadRequest, codeCommon, 701, "请输入正确的手机号码")
	RegistrationQQFmtErr           = new(statusBadRequest, codeCommon, 702, "请输入正确的QQ号")
	RegistrationPasswdFmtErr       = new(statusBadRequest, codeCommon, 703, "密码格式不符合要求，请至少包含英文、数字以及长度介于8-16码")
	PasswdNotEqual                 = new(statusBadRequest, codeCommon, 704, "两次密码输入不相等")
	QQEmpty                        = new(statusBadRequest, codeCommon, 705, "请填写QQ号")
	RegistrationConfirmPasswdEmpty = new(statusBadRequest, codeCommon, 706, "请填写确认密码")
	MobileEmpty                    = new(statusBadRequest, codeCommon, 707, "请填写手机号")
	PasswdEmpty                    = new(statusBadRequest, codeCommon, 708, "请填写密码")
	NewPasswdEmpty                 = new(statusBadRequest, codeCommon, 709, "请填写新密码")
	NewConfirmPasswdEmpty          = new(statusBadRequest, codeCommon, 710, "请填写确认新密码")
	NameEmpty                      = new(statusBadRequest, codeCommon, 711, "请填写中文姓名")
	RegistrationNameFmtErr         = new(statusBadRequest, codeCommon, 712, "只接受中文姓名，并且长度在20字内")
	DataNotQualified               = new(statusBadRequest, codeCommon, 713, "资料有误")
)

// member error code, begin of 1000.
var (
	AuthTokenCreateFailed    = new(statusBadRequest, codeAuth, 1, "权证取得失败")
	AuthTokenVerifyFailed    = new(statusBadRequest, codeAuth, 2, "权证验证失败")
	AuthTokenUnauthorized    = new(statusUnauthorized, codeAuth, 3, "未认证")
	AuthAccountOrPasswdError = new(statusUnauthorized, codeAuth, 4, "帐号/密码错误")
	AuthOperationForbidden   = new(statusForbidden, codeAuth, 5, "没有操作权限")
	AuthLogoutFailed         = new(statusForbidden, codeAuth, 6, "登出失败")
	AuthAccountDisable       = new(statusUnauthorized, codeAuth, 7, "帐号未启用")
	AuthLoginModeError       = new(statusForbidden, codeAuth, 8, "请求与登入模式冲突")
	AuthTokenGetFailed       = new(statusForbidden, codeAuth, 9, "无法取得token")
	AuthTokenDelFailed       = new(statusBadRequest, codeAuth, 10, "权证删除失败")
)

// Auth error code, begin of 2000.
var (
	LoginMemberSendVerificationCodeFetchTooFast  = new(statusBadRequest, codeAuth, 101, "不可连续取得验证码")
	LoginMemberIdentifierFailed                  = new(statusBadRequest, codeAuth, 102, "验证失败，请确认验证码是否正确")
	LoginMemberIdentifierVerificationCodeInvalid = new(statusBadRequest, codeAuth, 103, "验证码过期")
	LoginMemberSendVerificationTooManyTime       = new(statusBadRequest, codeAuth, 104, "取得验证码次数过多，请稍后再试")
	LoginMemberReverseVerificationFailed         = new(statusBadRequest, codeAuth, 105, "反转验证失败")
	LoginMemberReverseVerificationCodeError      = new(statusBadRequest, codeAuth, 106, "验证码不正确，或者验证码与手机号不相符")
	LoginMemberNoReceiveVerificationCode         = new(statusBadRequest, codeAuth, 107, "未收到验证码")
	LoginMemberRegistFailed                      = new(statusBadRequest, codeAuth, 108, "注册失败")
	LoginMemberRegistDuplicated                  = new(statusBadRequest, codeAuth, 109, "重复注册")
)

// policy
var (
	PolicyNotExist = new(statusBadRequest, codePolicy, 1, "规则不存在")
	PolicyExist    = new(statusBadRequest, codePolicy, 2, "规则已存在")
)
