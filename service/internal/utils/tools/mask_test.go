package tools

import "testing"

func TestMaskPhoneNumber(t *testing.T) {
	type args struct {
		number string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "len 4",
			args: args{number: "1234"},
			want: "*****",
		},
		{
			name: "len 5",
			args: args{number: "12345"},
			want: "*****",
		},
		{
			name: "len 6",
			args: args{number: "123456"},
			want: "1*****",
		},
		{
			name: "len 11",
			args: args{number: "12345678901"},
			want: "12*****8901",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MaskPhoneNumber(tt.args.number); got != tt.want {
				t.Errorf("MaskPhoneNumber() = %v, want %v", got, tt.want)
			}
		})
	}
}
