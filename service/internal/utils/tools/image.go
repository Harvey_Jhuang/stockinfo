package tools

import (
	"bytes"
	"image"

	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
)

// ImageFormat return the format of imageData
func ImageFormat(imageData []byte) (string, error) {
	_, format, err := image.Decode(bytes.NewReader(imageData))
	if err != nil {
		return "", err
	}
	return format, nil
}
