package tools

import "time"

func TimeNow() time.Time {
	return time.Now().Truncate(time.Second)
}
