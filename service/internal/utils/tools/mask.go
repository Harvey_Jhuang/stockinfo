package tools

func MaskPhoneNumber(number string) string {
	mask := "*****"

	numberLen := len(number)
	if numberLen > 5 {
		idx := numberLen / 4
		return number[:idx] + mask + number[idx+len(mask):]
	}

	return mask
}
