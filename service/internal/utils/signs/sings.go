package signs

import (
	"bytes"
	"crypto/sha512"
	"fmt"

	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/conf"
)

func Hash(data []byte) [64]byte {
	var buf bytes.Buffer
	buf.Write(data)
	buf.Write(conf.Sign().GetSalt())

	return sha512.Sum512(buf.Bytes())
}

func Hex(args ...interface{}) string {
	s := fmt.Sprint(args...)
	h := fmt.Sprintf("%x", Hash([]byte(s)))
	logger.ApLog().Debug(s)
	logger.ApLog().Debug(h)
	return h
}
