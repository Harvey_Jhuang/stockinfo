package ctxs

import (
	"context"

	"github.com/gin-gonic/gin"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/errs"
)

type keyName int

const (
	selfContext = "zqb_self_context"
)

const (
	memberID keyName = iota
	adminID
	sessionID
	merchantID
	tokenID
)

func New() context.Context {
	return context.Background()
}

func SetSelfContext(ctx context.Context, sc interface{}) interface{} {
	switch t := sc.(type) {
	case *gin.Context:
		t.Set(selfContext, ctx)
	default:
		logger.ApLog().Panicf("%s, type: %v", errs.FrameworkContextErrorType, t)
	}

	return sc
}

func GetSelfContext(ctx interface{}) context.Context {
	var iSelf interface{}

	switch t := ctx.(type) {
	case *gin.Context:
		if v, ok := t.Get(selfContext); ok {
			iSelf = v
		}
	default:
		logger.ApLog().Panicf("%s, type: %v", errs.FrameworkContextErrorType, t)
	}

	if c, ok := iSelf.(context.Context); ok {
		return c
	}

	return New()
}

func SetSessionID(ctx context.Context, id string) context.Context {
	return context.WithValue(ctx, sessionID, id)
}

func GetSessionID(ctx context.Context) (string, bool) {
	val := ctx.Value(sessionID)

	v, ok := val.(string)
	if !ok {
		return "", false
	}

	return v, true
}

func SetMemberID(ctx context.Context, id int64) context.Context {
	return context.WithValue(ctx, memberID, id)
}

func GetMemberID(ctx context.Context) (int64, bool) {
	val := ctx.Value(memberID)

	v, ok := val.(int64)
	if !ok {
		return 0, false
	}

	return v, true
}

func SetAdminID(ctx context.Context, id int64) context.Context {
	return context.WithValue(ctx, adminID, id)
}

func GetAdminID(ctx context.Context) (int64, bool) {
	val := ctx.Value(adminID)

	v, ok := val.(int64)
	if !ok {
		return 0, false
	}

	return v, true
}

func SetMerchantID(ctx context.Context, id string) context.Context {
	return context.WithValue(ctx, merchantID, id)
}

func GetMerchantID(ctx context.Context) (string, bool) {
	val := ctx.Value(merchantID)

	v, ok := val.(string)
	if !ok {
		return "", false
	}
	return v, true
}

func SetToken(ctx context.Context, token string) context.Context {
	return context.WithValue(ctx, tokenID, token)
}

func GetToken(ctx context.Context) (string, bool) {
	val := ctx.Value(tokenID)

	v, ok := val.(string)
	if !ok {
		return "", false
	}

	return v, true
}
