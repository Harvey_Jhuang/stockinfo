package condition

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models"
)

type PolicyCond struct {
	Role string `gorm:"v0" form:"role" json:"role"`
	models.Paging
}
