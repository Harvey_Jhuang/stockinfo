package condition

type StockJobConfig struct {
	Path string
	Date string
	Key  string
}

type StockJobReq struct {
	Path *string `json:"path" form:"path"`
	Date *string `json:"date" form:"date"`
	Key  *string `json:"key" form:"key"`
}
