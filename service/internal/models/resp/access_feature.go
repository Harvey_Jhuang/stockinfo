package resp

import "gitlab.com/Harvey_Jhuang/stockinfo/service/internal/models/model"

type GetFeatureListResp struct {
	GroupCode string           `json:"group_code"`
	GroupName string           `json:"group_name"`
	Features  []*model.Feature `json:"features"`
}
