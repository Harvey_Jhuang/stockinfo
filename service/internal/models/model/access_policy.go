package model

type PolicyInfo struct {
	ID          string `gorm:"id" form:"id" json:"id"`
	Name        string `gorm:"name" form:"name" json:"name"`
	Description string `gorm:"description" form:"description" json:"description"`
}

func (PolicyInfo) TableName() string {
	return "access_policy_info"
}

type Policy struct {
	Role        string `gorm:"v0" form:"role" json:"role"`
	Path        string `gorm:"v1" form:"path" json:"path"`
	Method      string `gorm:"v2" form:"method" json:"method"`
	Description string `gorm:"description" form:"description" json:"description"`
}

type PolicyDetail struct {
	ID          string  `gorm:"id" form:"id" json:"-"`
	Name        string  `gorm:"name" form:"name" json:"name"`
	Description string  `gorm:"description" form:"description" json:"description"`
	Features    []int32 `gorm:"features" form:"features" json:"features"`
}
