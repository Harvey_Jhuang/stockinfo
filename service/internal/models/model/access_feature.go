package model

import "time"

type Feature struct {
	ID        int    `gorm:"id" form:"id" json:"id"`
	Name      string `gorm:"name" form:"name" json:"name"`
	Path      string `gorm:"path" form:"path" json:"path"`
	Method    string `gorm:"method" form:"method" json:"method"`
	GroupCode string `gorm:"group_code" form:"group_code" json:"group_code"`
}

func (Feature) TableName() string {
	return "access_feature_list"
}

type FeatureGroup struct {
	Code        string
	Name        string
	AddedTime   time.Time
	UpdatedTime time.Time
}

func (FeatureGroup) TableName() string {
	return "access_feature_group"
}
