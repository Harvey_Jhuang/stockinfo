package model

import "time"

type StockCloseInfo struct {
	Code            string    `gorm:"column:code,PRIMARY_KEY"`   //證券代號
	Name            string    `gorm:"column:name"`               //證券名稱
	Date            time.Time `gorm:"column:date,PRIMARY_KEY"`   //日期
	NumberOfShare   int64     `gorm:"column:number_of_share"`    //成交股數
	NumberOfDeal    int64     `gorm:"column:number_of_deal"`     //成交筆數
	AmountOfDeal    float64   `gorm:"column:amount_of_deal"`     //成交金額
	OpenPrice       float64   `gorm:"column:open_price"`         //開盤價
	HighPrice       float64   `gorm:"column:high_price"`         //最高價
	LowPrice        float64   `gorm:"column:low_price"`          //最低價
	ClosePrice      float64   `gorm:"column:close_price"`        //收盤價
	Trend           string    `gorm:"column:trend"`              //漲跌 "+"=漲;"-"=跌;" "=不變
	PriceDiff       float64   `gorm:"column:price_diff"`         //漲跌價差
	LastAsk         float64   `gorm:"column:last_ask"`           //最後揭示買價
	NumberOfLastAsk int64     `gorm:"column:number_of_last_ask"` //最後揭示買量
	LastBid         float64   `gorm:"column:last_bid"`           //最後揭示賣價
	NumberOfLastBid int64     `gorm:"column:number_of_last_bid"` //最後揭示賣量
	PERatio         float64   `gorm:"column:pe_ratio"`           //本益比
}

func (StockCloseInfo) TableName() string {
	return "stock_close_info"
}

type MarketIndex struct {
	Name                   string    `gorm:"column:name"`                       //指數名稱
	Date                   time.Time `gorm:"column:date"`                       //日期
	Trend                  string    `gorm:"column:trend"`                      //漲跌 "+"=漲;"-"=跌;" "=不變
	ClosePrice             float64   `gorm:"column:close_price"`                //收盤價
	PriceDiff              float64   `gorm:"column:price_diff"`                 //漲跌價差
	PriceDiffRatio         float64   `gorm:"column:price_diff_ratio"`           //漲跌百分比
	NumberOfShare          int64     `gorm:"column:number_of_share"`            //成交股數
	NumberOfDeal           int64     `gorm:"column:number_of_deal"`             //成交筆數
	AmountOfDeal           float64   `gorm:"column:amount_of_deal"`             //成交金額
	NumberOfStockLimitUp   int64     `gorm:"column:number_of_stock_limit_up"`   //股票漲停總數
	NumberOfStockLimitDown int64     `gorm:"column:number_of_stock_limit_down"` //股票跌停總數
	NumberOfStockGoUp      int64     `gorm:"column:number_of_stock_go_up"`      //股票上漲總數
	NumberOfStockGoDown    int64     `gorm:"column:number_of_stock_go_down"`    //股票下漲總數
	NumberOfStockUnchanged int64     `gorm:"column:number_of_stock_unchanged"`  //股票平盤總數
	Remarks                string    `gorm:"column:remarks,type:text"`          //特殊處理註記
}

func (MarketIndex) TableName() string {
	return "market_index"
}
