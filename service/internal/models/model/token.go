package model

type AuthToken struct {
	ID    int64
	Token string
}
