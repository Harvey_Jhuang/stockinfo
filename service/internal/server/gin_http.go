package server

import (
	"runtime"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/ctxs"
)

func newGinServerHTTP(serve *GinServer) *ginServerHTTP {
	return &ginServerHTTP{
		GinServer: serve,
	}
}

type ginServerHTTP struct {
	*GinServer
}

func (gs *ginServerHTTP) Start() {
	gs.engine = gin.New()
	gs.globMiddleware()
	gs.loadTemplates()
	gs.clientRouter()
	gs.adminRouter()

	// debug
	runtime.SetBlockProfileRate(1)
	runtime.SetMutexProfileFraction(1)
	pprof.Register(gs.engine)
}

func (gs *ginServerHTTP) globMiddleware() {
	/*
		gin.DisableConsoleColor()
		// Logging to a file.
		f, _ := os.Create("gin.log")
		gin.DefaultWriter = io.MultiWriter(f)
	*/

	gs.engine.Use(gin.Logger())
	gs.engine.Use(gs.middlewareGenSelfContext)
	gs.engine.Use(gs.middlewareTraceInOut)
}

func (gs *ginServerHTTP) loadTemplates() {
	gs.engine.LoadHTMLGlob("templates/*.html")
	//gs.engine.Static("/api/v1/static", "static")
	gs.engine.Static("/static", "static")
}

func (gs *ginServerHTTP) clientRouter() {
	anonymous := gs.engine.Group("/api")
	anonymous.GET("/v1/health-check", gs.clientController.Syetem.HealthCheck)
	anonymous.GET("/v1/home", gs.clientController.Stock.MainPage)

	apis := gs.engine.Group("/api")
	apis.Use(gs.clientController.HttpAuthToken.MiddlewareAuth)

}

func (gs *ginServerHTTP) adminRouter() {
	anonymous := gs.engine.Group("/admin")
	anonymous.GET("/v1/health-check", gs.clientController.Syetem.HealthCheck)
	anonymous.POST("/v1/stock/info", gs.adminController.Stock.AddInfoJob)
	anonymous.POST("/v1/stock/index", gs.adminController.Stock.AddIndexJob)

	admins := gs.engine.Group("/admin")
	admins.Use(gs.adminController.HttpAuthToken.MiddlewareAuth)

	adminsAc := gs.engine.Group("/admin")
	adminsAc.Use(gs.adminController.HttpAuthToken.MiddlewareAuth)
	adminsAc.Use(gs.adminController.HttpAuthToken.MiddlewareRBAC)
}

func (gs *ginServerHTTP) middlewareGenSelfContext(ctx *gin.Context) {
	self := ctxs.New()
	self = ctxs.SetSessionID(self, uuid.New().String())

	ctxs.SetSelfContext(self, ctx)

	ctx.Next()
}

func (gs *ginServerHTTP) middlewareTraceInOut(ctx *gin.Context) {
	self := ctxs.GetSelfContext(ctx)
	sessionID, _ := ctxs.GetSessionID(self)
	routing := ctx.Request.Method + " " + ctx.Request.URL.Path

	logger.ApLog().Debugf("Req: [sessionID: %s], routing: %s",
		sessionID,
		routing)

	ctx.Next()

	logger.ApLog().Debugf("Resp: [sessionID: %s], routing: %s, status: %d",
		sessionID,
		routing,
		ctx.Writer.Status())
}
