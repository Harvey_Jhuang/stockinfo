package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/adminctl"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/controller/clientctl"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/conf"
	"go.uber.org/dig"
)

func NewGinServer(set ginServeSet) IGinServer {
	return &GinServer{
		clientController: set.AppController,
		adminController:  set.AdminController,
	}
}

type IGinServer interface {
	Run()
}

type GinServer struct {
	clientController *clientctl.Controller
	adminController  *adminctl.Controller
	engine           *gin.Engine
}

type ginServeSet struct {
	dig.In

	AppController   *clientctl.Controller
	AdminController *adminctl.Controller
}

func (gs *GinServer) Run() {
	newGinServerHTTP(gs).Start()

	gs.engine.Run(conf.Service().GetApisHttpPort())
}
