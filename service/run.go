package service

type Serve int

const (
	ServerGin Serve = iota
)

var (
	serverStrategy = map[Serve]IService{
		ServerGin: newService(),
	}
)

type IService interface {
	Run()
}

func Run(serv Serve) {
	obj, ok := serverStrategy[serv]
	if !ok {
		panic("server strategy not found")
	}
	obj.Run()

	select {}
}
