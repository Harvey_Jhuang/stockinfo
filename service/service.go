package service

import (
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/logger"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/app/worker"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/binder"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/server"
	"gitlab.com/Harvey_Jhuang/stockinfo/service/internal/utils/conf"
	"go.uber.org/dig"
)

func newService() IService {
	return &GinServer{}
}

type GinServer struct {
}

func (gs GinServer) Run() {
	gs.preStart()

	binder := binder.New()
	if err := binder.Invoke(gs.gen); err != nil {
		panic(err)
	}
}

func (gs GinServer) preStart() {
	conf.Start()
	logger.Start()
}

func (gs GinServer) gen(set ginServeSet) {
	logger.ApLog().Info("GinServer gen start")
	go worker.Cron().Start()
	go set.GinServer.Run()
}

type ginServeSet struct {
	dig.In

	GinServer server.IGinServer
}
