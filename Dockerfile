FROM alpine:3.10 AS base
RUN apk update && \
    apk upgrade && \
    apk add --no-cache ca-certificates

FROM golang:1.12-alpine AS go-builder
RUN apk add git
WORKDIR /app
COPY . .
RUN go build -o stockinfo /app/cmd/

FROM base
WORKDIR /stock
COPY --from=go-builder /app/stockinfo /stock/stockinfo
COPY --from=go-builder /app/conf.d /stock/conf.d
ENTRYPOINT ["./stockinfo"]
