# 存錢寶 docker 環境部署

### 主機資訊
| 環境    | IP             |  主機名        |  備註   |
| :----   | :----          | :----         |  :----  |
| DEV    | 10.200.252.216  |  dev-db1      | zqb 備份來源                 |
| DEV    | 10.200.252.199  |  dev-dba-jump | zqb 備份腳本與存放 zqb 備份檔 |
| DEV    | 10.200.252.112  |  god-sicaj    | 存放 zqb 備份檔(RD 機器)     |
| DEV    | 10.200.252.185  |  dev-zqb      | zqb 開發環境(RD 機器) |

### 目錄結構
<pre>
# god-sicaj 目錄
dba_tw@god-sicaj[/usr/local/backup/zqb]# tree .
.
└── 20200703
    ├── zqb_all.sql
    ├── zqb_only_data.sql
    └── zqb_only_schema.sql

# 開發環境需創建目錄，並包含以下檔案
├── docker-compose.yaml
├── redis
│  └── redis.conf
└── sql
    └── zqb_all.sql

1. 其中 zqb_all.sql 來源為 god-sicaj
2. redis.conf 為針對 dev-zqb 規格調降 maxmemory 之config
</pre>

### zqb MySQL 備份流程
1. dev-dba-jump 執行備份，備份腳本: /usr/local/DBA/script/routine/zqb_bak/zqb_bak.sh
2. dev-dba-jump 備份完成後將備份檔 scp 至 god-sicaj
3. 開發者從 god-sicaj 將最新的 zqb sql 下載，搭配附件的 docker-compose.yaml，即可快速啟動一個 MySQL zqb 環境
