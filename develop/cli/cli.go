package main

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logger = newLogger(zapcore.DebugLevel)

func main() {
	now := time.Now()
	loc := now.Location()
	now = time.Date(2016, time.January, 1, 12, 59, 59, 999, loc)

	for now.After(time.Date(2014, time.December, 31, 23, 59, 59, 999, loc)) {
		date := now.Format("20060102")
		num := time.Duration(rand.Intn(5) + 3)
		num2 := time.Duration(rand.Intn(2) + 1)

		if now.Weekday() == time.Saturday || now.Weekday() == time.Sunday {
			now = now.AddDate(0, 0, -1)
			continue
		}

		logger.Infof("start crawler %s data after %d second", date, num)

		time.Sleep(num * time.Second)
		addStockCloseInfoRecord(date)

		time.Sleep(num2 * time.Second)
		addMarketIndexRecord(date)

		logger.Infof("crawling  %s data done", date)

		now = now.AddDate(0, 0, -1)
	}

}

func addStockCloseInfoRecord(date string) {
	req, err := json.Marshal(map[string]string{
		"date": date,
	})
	if err != nil {
		logger.Error(err)
	}

	resp, err := http.Post("http://127.0.0.1:17801/admin/v1/stock/info", "application/json", bytes.NewBuffer(req))
	if err != nil {
		logger.Errorf("crawling close info  at %s failed = %v", date, err)
	}

	if resp.Status != "200 OK" {
		logger.Errorf("crawling close info  at %s status = %s ", date, resp.Status)
	}

	defer resp.Body.Close()
}

func addMarketIndexRecord(date string) {
	req, err := json.Marshal(map[string]string{
		"date": date,
	})
	if err != nil {
		logger.Error(err)
	}

	resp, err := http.Post("http://127.0.0.1:17801/admin/v1/stock/index", "application/json", bytes.NewBuffer(req))
	if err != nil {
		logger.Errorf("crawling market index at %s failed = %v", date, err)
	}

	if resp.Status != "200 OK" {
		logger.Errorf("crawling market index at %s status = %s", date, resp.Status)
	}
	defer resp.Body.Close()
}

func newLogger(level zapcore.Level) *zap.SugaredLogger {
	writeSyncer := getLogWriter()
	encoder := getEncoder()
	core := zapcore.NewCore(encoder, writeSyncer, level)
	logger := zap.New(core, zap.AddCaller())
	return logger.Sugar()
}

func getLogWriter() zapcore.WriteSyncer {
	return zapcore.AddSync(os.Stdout)
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	// package/file:line
	encoderConfig.EncodeCaller = zapcore.ShortCallerEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
}
