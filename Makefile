#!make

# Go parameters
BUILD_ENV=CGO_ENABLED=0
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOMOD=$(GOCMD) mod
GOVET=$(GOCMD) vet
TARGET_EXEC=detto

all: clean set_up test build run

clean: 
	$(GOCLEAN)
	rm -rf build/linux
	rm -rf build/osx
	rm -rf build/windows
	rm -rf build/local

set_up:
	mkdir -p build/linux
	mkdir -p build/osx
	mkdir -p build/windows
	mkdir -p build/local

download:
	$(GOMOD) download

test: 
	$(GOTEST) -v ./...

vet:
	$(GOVET) -all ./...

build: clean set_up
	$(GOBUILD) -o build/local/$(TARGET_EXEC) -v ./main.go

run: clean set_up build
	./build/local/$(TARGET_EXEC)

traceGC: clean set_up build
	GODEBUG=gctrace=1 ./build/local/$(TARGET_EXEC)

# Cross compilation
build-linux: clean set_up
	GOOS=linux $(GOBUILD) -o build/linux/${TARGET_EXEC} -v ./main.go
build-osx: set_up
	GOOS=darwin $(GOBUILD) -o build/osx/${TARGET_EXEC} -v ./main.go
build-windows: setup
	GOOS=windows $(GOBUILD) -o build/windows/${TARGET_EXEC}.exe -v ./main.go

