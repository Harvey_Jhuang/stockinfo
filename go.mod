module gitlab.com/Harvey_Jhuang/stockinfo

go 1.13

require (
	github.com/ahmetb/go-linq/v3 v3.1.0
	github.com/aws/aws-sdk-go v1.34.13
	github.com/bwmarrin/snowflake v0.3.0
	github.com/casbin/casbin/v2 v2.11.2
	github.com/casbin/gorm-adapter/v2 v2.1.0
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis/v7 v7.4.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.16
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron/v3 v3.0.1
	github.com/spf13/viper v1.7.1
	go.uber.org/dig v1.10.0
	go.uber.org/zap v1.15.0
)
